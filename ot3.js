class Traveler{
    constructor(name, food, isHealthy){
        this.name = name;
        this.food = 1;
        this.isHealthy = true;
    }
    hunt(){
        return this.food+=2
    }
    eat(){
        this.food-=1;
        if(this.food <= 1){
        this.isHealthy = false
        }
    }

}

class Wagon {
    constructor(capacity, passengers){
    this.capacity = capacity;
    this.passengers = [];
    }

    emptySeats(){
       return this.capacity - this.passengers.length
}
    getAvailableSeatCount(){
        return this.capacity - this.passengers.length
}
    join(Traveler){
        let array = this.passengers 
        if (this.emptySeats() > 0){
        array.push(Traveler)
        return array
        }else{
        // console.log("There is no more room!")
        }
    }
    shouldQuarantine(){
        for(let i=0;i<this.passengers.length;i++){
            if(this.passengers[i].isHealthy === true){
                return false
            }else{
                return true
            }
        }
    }
    totalFood(){
        let foodTotal = 0;
        for(let i=0;i<this.passengers.length;i++){
            foodTotal += this.passengers[i].food
            }
            return foodTotal
    }
}

 

class Doctor extends Traveler {
    constructor(name, food, isHealthy){
    super(name, food, isHealthy)
    this.food = 2;
    }
    heal(){
        if(Traveler.isHealthy === false){
            Traveler.isHealthy = true
            // console.log("You are healed!")
            return
            }else{
            // console.log("No need for my help")
            return
        }
    }
    hunt(){
        return this.food+=2
    }
    eat(){
        if(this.food <= 1){
            this.isHealthy = false
            return this.food = 0
            // console.log(this.food)
           }else{
            return this.food-=1
           }
        //    console.log(this.food)
    }
    }


class Hunter extends Traveler {
    constructor(name, food, isHealthy){
        super(name, food, isHealthy)
    }
    giveFood(Traveler, numOfFoodUnits){
        if(Hunter.food>Traveler.food){
            let diff = Hunter.food - numOfFoodUnits
            Traveler.food = diff + Traveler.food
            return Traveler.food
        }else{
            // console.log("No can do")
        }
    }
    hunt(){
        return this.food+=5
    }
    eat(){
        if(this.food < 2){
            this.isHealthy = false
            return this.food = 0
            // console.log(this.food)
           }else{
            return this.food-=2
           }
        //    console.log(this.food)
    }
}
 
// Create a wagon that can hold 4 people
let wagon = new Wagon(4);
// Create five travelers
let henrietta = new Traveler('Henrietta');
let juan = new Traveler('Juan');
let drsmith = new Doctor('Dr. Smith');
let sarahunter = new Hunter('Sara');
let maude = new Traveler('Maude');
console.log(`#1: There should be 4 available seats. Actual: ${wagon.getAvailableSeatCount()}`);
wagon.join(henrietta);
console.log(`#2: There should be 3 available seats. Actual: ${wagon.getAvailableSeatCount()}`);
wagon.join(juan);
wagon.join(drsmith);
wagon.join(sarahunter);
wagon.join(maude); // There isn't room for her!
console.log(`#3: There should be 0 available seats. Actual: ${wagon.getAvailableSeatCount()}`);
console.log(`#4: There should be 5 total food. Actual: ${wagon.totalFood()}`);
sarahunter.hunt(); // gets 5 more food
drsmith.hunt();
console.log(`#5: There should be 12 total food. Actual: ${wagon.totalFood()}`);
henrietta.eat();
sarahunter.eat();
drsmith.eat();
juan.eat(); // juan is now hungry (sick)
console.log(`#6: Quarantine should be true. Actual: ${wagon.shouldQuarantine()}`);
console.log(`#7: There should be 7 total food. Actual: ${wagon.totalFood()}`);
drsmith.heal(juan);
console.log(`#8: Quarantine should be false. Actual: ${wagon.shouldQuarantine()}`);
sarahunter.giveFood(juan, 4);
sarahunter.eat(); // She only has 1, so she eats it and is now sick
console.log(`#9: Quarantine should be true. Actual: ${wagon.shouldQuarantine()}`);
console.log(`#10: There should be 6 total food. Actual: ${wagon.totalFood()}`);